## Ejercicio - Tablas de multiplicar

La tabla de multiplicación se enseña tradicionalmente como una parte esencial de la aritmética elemental en todo el mundo, ya que sienta las bases para las operaciones aritméticas con números en base diez.


Crea el método `multiplication_tables` que recibe como parámetro un número entero e imprime las tablas de multiplicar desde el 1 hasta el número que le pasen.

Formatea las tablas de multiplicar como en los ejemplos:

```ruby
multiplication_tables(5)
=>
1    2    3    4    5    6    7    8    9    10
2    4    6    8    10   12   14   16   18   20
3    6    9    12   15   18   21   24   27   30
4    8    12   16   20   24   28   32   36   40
5    10   15   20   25   30   35   40   45   50

multiplication_tables(7)
=>
1    2    3    4    5    6    7    8    9    10
2    4    6    8    10   12   14   16   18   20
3    6    9    12   15   18   21   24   27   30
4    8    12   16   20   24   28   32   36   40
5    10   15   20   25   30   35   40   45   50
6    12   18   24   30   36   42   48   54   60
7    14   21   28   35   42   49   56   63   70
```
